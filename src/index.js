import React from "react";
import ReactDOM from "react-dom/client";

import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import themeSlice from "./redux/reduxToolkit/themeSlice";
import chatSlice from "./redux/reduxToolkit/chatSlice";
import configSlice from "./redux/reduxToolkit/configSlice";
import userSlice from "./redux/reduxToolkit/userSlice";
import pageSlice from "./redux/reduxToolkit/pageSlice";
const root = ReactDOM.createRoot(document.getElementById("root"));

export const store = configureStore({
  reducer: {
    themeSlice,
    chatSlice,
    pageSlice,
    configSlice,
    userSlice
  },
  // devTools: false,
});

root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
