import React from "react";
import style from "./chatBot.module.css";
import ContentChat from "./MainChat/ContentChat";
import InputChat from "./MainChat/InputChat";

function ChatBot() {
  return (
    <div className={style.container_chatbot}>
      <ContentChat />
      <InputChat />
    </div>
  );
}

export default ChatBot;
