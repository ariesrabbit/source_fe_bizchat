import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import TypingEffect from "../../../../components/typingEffect/TypingEffect";
import {
  setArrIdQuiz,
  setConversation,
  setPropExpendConversation,
  setQuestion,
  setThinking,
} from "../../../../redux/reduxToolkit/chatSlice";
import { Icon } from "../../../../assets/icon_svg/icon";
import Checkbox from "@mui/material/Checkbox";
import style from "./contentChat.module.css";
import { IconButton, Snackbar, SnackbarContent } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { idUserLocal } from "../../../../services/localServices";
import { chatService } from "../../../../services/ChatService";
import QuizGame from "../../../../components/QuizGame/QuizGame";
const ResTimeOut =
  "I’m sorry for the delay in my response. It seems like I'm experiencing a technical issue, but our team is doing their best to resolve it. Please refresh the page and try again. Thank you for your patience!";

function ContentChat() {
  const dispatch = useDispatch();
  const divRef = useRef();
  const thinking = useSelector((state) => state.chatSlice.thinking);
  const detailBotId = useSelector((state) => state.chatSlice.detailBotId);
  const conversation = useSelector((state) => state.chatSlice.conversation);

  const AccessUrl = (url) => {
    window.open(url, "_blank", "noopener,noreferrer");
  };

  useEffect(() => {
    setTimeout(() => {
      divRef.current.scrollTop = divRef.current.scrollHeight;
    }, 1);
  }, []);

  useEffect(() => {
    divRef.current.scrollTop = divRef.current.scrollHeight;
  }, [conversation, thinking]);
  const ExpendUrl = (idx) => {
    dispatch(setPropExpendConversation(idx));
  };

  const isYoutubeLink = (link) => {
    const regex = /^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/;
    return regex.test(link);
  };
  const getYoutubeId = (url) => {
    const regex =
      /(?:youtube.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu.be\/)([^"&?/\s]{11})/;
    const match = url.match(regex);
    if (match) {
      return match[1];
    }
    return "";
  };

  const [open, setOpen] = useState(false);

  const handleClickCopy = () => {
    setOpen(true);
  };

  const handleCloseCopy = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const action = (
    <IconButton
      size="small"
      aria-label="close"
      color="inherit"
      onClick={handleCloseCopy}
    >
      <CloseIcon fontSize="small" />
    </IconButton>
  );
  const copyToClipboard = async (text) => {
    try {
      await navigator.clipboard.writeText(text);
    } catch (err) {
      console.log(err);
    }
  };
  const handleFaviconError = (event) => {
    event.target.src = "https://billybot.ai/media/billy-prod/dummy_favicon.jpg";
  };
  //start phần để bấm sugg nó bung chat luôn
  let getAnswer = async (requirement) => {
    try {
      let res = await chatService.postChat(requirement);
      if (res) {
        //console.log(res.reference_link)
        dispatch(
          setConversation({
            type: "awnser",
            text: res.text?.length > 0 ? res.text : ResTimeOut,
            links: res.reference_link,
            topic_suggestions: res.topic_suggestions,
            quizgame: res.quizgame,
            isExpend: false,
            isLoadLink: false,
          })
        );
      } else {
        dispatch(
          setConversation({
            type: "awnser",
            text: ResTimeOut,
            links: [],
            topic_suggestions: [],
            quizgame: false,
            isExpend: false,
            isLoadLink: false,
          })
        );
      }
      dispatch(setThinking(false));
    } catch (error) {}
  };

  let handleTopic = (topic) => {
    dispatch(setThinking(true));
    dispatch(
      setConversation({
        type: "question",
        text: topic,
      })
    );
    getAnswer({
      query: topic,
      bot_id: detailBotId.id,
      user_id: idUserLocal.get(),
    });
  };
  //end phần để bấm sugg nó bung chat luôn

  // start tạo quiz game

  let fetchApiQuiz = async (answer) => {
    let res = await chatService.postQuizGame({
      context: answer,
      n_quiz: 3,
      config: {},
    });
    dispatch(
      setConversation({
        type: "quizgame",
        text: res.quiz,
      })
    );
    dispatch(setThinking(false));
  };

  // end tạo quiz game
  let renderConversation = () => {
    return (
      <>
        {conversation?.map((mess, index) => (
          <>
            {mess.type === "question" ? (
              <div key={mess.id} className={style.lineChatClient}>
                <div className={style.fm_chat}>
                  <p className="break-words">{mess.text}</p>
                </div>
              </div>
            ) : (
              <>
                {mess.type === "awnser" ? (
                  <>
                    <div key={mess.id} className={style.lineChatBot}>
                      <div className={style.avt}>
                        <img src={detailBotId.avatar} alt="" />
                      </div>
                      <div
                        className={style.fm_chat}
                        style={
                          window.innerWidth <= 900
                            ? null
                            : mess.text?.length <= 200
                            ? { maxWidth: "60%" }
                            : { maxWidth: "70%" }
                        }
                      >
                        {index === conversation?.length - 1 ? (
                          <>
                            <TypingEffect
                              divRef={divRef}
                              content={mess.text}
                              idx={mess.id}
                            />
                            {mess.isLoadLink ? (
                              <>
                                {index === 0 ? null : (
                                  <>
                                    {mess.topic_suggestions?.length > 0 ? (
                                      <p>
                                        I have some interesting topics that I'd
                                        like to share with you:
                                      </p>
                                    ) : null}
                                  </>
                                )}
                                {mess.topic_suggestions?.length > 0 ? (
                                  <div className="flex flex-wrap">
                                    {mess.topic_suggestions?.map(
                                      (topic, index) => {
                                        return (
                                          <>
                                            <button
                                              className={style.button_topic}
                                              onClick={() => {
                                                handleTopic(topic.value);
                                              }}
                                              key={index}
                                              disabled={thinking}
                                            >
                                              {topic.topic}
                                            </button>
                                          </>
                                        );
                                      }
                                    )}
                                  </div>
                                ) : null}
                                {index === 0 ? null : (
                                  <div className="flex justify-between items-end mt-4">
                                    {mess.quizgame ? (
                                      <>
                                        <button
                                          onClick={() => {
                                            fetchApiQuiz(mess.text);
                                            dispatch(setThinking(true));
                                          }}
                                          className={style.quiz}
                                        >
                                          {Icon.ChatBubbleQuestion}Quiz game
                                        </button>
                                      </>
                                    ) : (
                                      <div></div>
                                    )}
                                    <div className="flex justify-center items-center">
                                      <div
                                        className="cursor-pointer"
                                        onClick={() => {
                                          copyToClipboard(mess.text);
                                          handleClickCopy();
                                        }}
                                      >
                                        <Checkbox
                                          style={{ color: "white" }}
                                          icon={Icon.Copy}
                                          checkedIcon={Icon.Copy}
                                        />
                                      </div>
                                      <div>
                                        <Checkbox
                                          style={{ color: "white" }}
                                          icon={Icon.Like}
                                          checkedIcon={Icon.Liked}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                )}
                              </>
                            ) : null}
                          </>
                        ) : (
                          <>
                            <p className="break-words">{mess.text}</p>
                            {index === 0 ? null : (
                              <>
                                {mess.topic_suggestions?.length > 0 ? (
                                  <p>
                                    I have some interesting topics that I'd like
                                    to share with you:
                                  </p>
                                ) : null}
                              </>
                            )}
                            {mess.topic_suggestions?.length > 0 ? (
                              <div className="flex flex-wrap">
                                {mess.topic_suggestions?.map((topic, index) => {
                                  return (
                                    <>
                                      <button
                                        className={style.button_topic}
                                        onClick={() => {
                                          handleTopic(topic.value);
                                        }}
                                        key={index}
                                        disabled={thinking}
                                      >
                                        {topic.topic}
                                      </button>
                                    </>
                                  );
                                })}
                              </div>
                            ) : null}
                            {index === 0 ? null : (
                              <div className="flex justify-between items-end mt-4">
                                {mess.quizgame ? (
                                  <>
                                    <button
                                      onClick={() => {
                                        fetchApiQuiz(mess.text);
                                        dispatch(setThinking(true));
                                      }}
                                      className={style.quiz}
                                      disabled={thinking}
                                    >
                                      {Icon.ChatBubbleQuestion}Quiz game
                                    </button>
                                  </>
                                ) : (
                                  <div></div>
                                )}
                                <div className="flex justify-center items-center">
                                  <div
                                    className="cursor-pointer"
                                    onClick={() => {
                                      copyToClipboard(mess.text);
                                      handleClickCopy();
                                    }}
                                  >
                                    <Checkbox
                                      style={{ color: "white" }}
                                      icon={Icon.Copy}
                                      checkedIcon={Icon.Copy}
                                    />
                                  </div>
                                  <div>
                                    <Checkbox
                                      style={{ color: "white" }}
                                      icon={Icon.Like}
                                      checkedIcon={Icon.Liked}
                                    />
                                  </div>
                                </div>
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </div>

                    {mess.isLoadLink && mess.links?.length > 0 ? (
                      <>
                        <div className={style.referenceLink}>
                          <div className={style.titleInput}>
                            <div className={style.d1}>
                              This answer was compiled from these sources:
                            </div>
                            <div
                              className={style.d2}
                              onClick={() => ExpendUrl(mess.id)}
                            >
                              {window.innerWidth <= 900 ? null : (
                                <span>View detailed</span>
                              )}
                              {!mess.isExpend ? (
                                <span>{Icon.Expend}</span>
                              ) : (
                                <span>{Icon.PreExpend}</span>
                              )}
                            </div>
                          </div>

                          <div className={style.boxLink}>
                            {mess.links.map((link, index) => (
                              <>
                                {!mess.isExpend ? (
                                  <div
                                    onClick={() => AccessUrl(link.url)}
                                    key={index}
                                    className={style.smallLink}
                                  >
                                    <img
                                      src={link.icon}
                                      onError={handleFaviconError}
                                      alt=""
                                    />
                                    <p
                                      className={style.overfow_text}
                                      href={link.url}
                                    >
                                      {link.domain}
                                    </p>
                                    <span className={style.accessLink}>
                                      {Icon.Share}
                                    </span>
                                  </div>
                                ) : (
                                  <div key={index} className={style.largeLink}>
                                    <div className={style.h1_link}>
                                      <div>
                                        <img
                                          src={link.icon}
                                          onError={handleFaviconError}
                                          alt=""
                                        />
                                        <span>{link.domain}</span>
                                      </div>
                                      <div
                                        onClick={() => AccessUrl(link.url)}
                                        className={style.accessLink}
                                      >
                                        <div>{Icon.Share}</div>
                                      </div>
                                    </div>
                                    <div
                                      style={{
                                        margin: "10px 5px 0 3px",
                                        maxWidth: "90%",
                                        overflow: "auto",
                                      }}
                                    >
                                      {link.content}
                                      <br />
                                      <br />
                                      {isYoutubeLink(link.url) ? (
                                        <div className="w-full">
                                          <iframe
                                            src={`https://www.youtube.com/embed/${getYoutubeId(
                                              link.url
                                            )}`}
                                            className="aspect-video w-2/3"
                                            frameborder="0"
                                            allowfullscreen="allowfullscreen"
                                            mozallowfullscreen="mozallowfullscreen"
                                            msallowfullscreen="msallowfullscreen"
                                            oallowfullscreen="oallowfullscreen"
                                            webkitallowfullscreen="webkitallowfullscreen"
                                          ></iframe>
                                        </div>
                                      ) : null}
                                    </div>
                                    {index !== mess.links?.length - 1 ? (
                                      <div className={style.div_border}></div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                )}
                              </>
                            ))}
                          </div>
                        </div>
                      </>
                    ) : null}
                  </>
                ) : (
                  <>
                    {mess.type === "quizgame" ? (
                      <div className={style.lineChatBot}>
                        <div className={style.avt}>
                          <img src={detailBotId.avatar} alt="" />
                        </div>
                        <div
                          style={{ width: "70%", maxWidth: "70%" }}
                          className={style.fm_chat}
                        >
                          <QuizGame quiz={mess.text} idQuiz={mess.id} />
                        </div>
                      </div>
                    ) : null}
                  </>
                )}
              </>
            )}
          </>
        ))}

        {thinking ? (
          <>
            <div key="thingking" className={style.lineChatBot}>
              <div className={style.avt}>
                <img src={detailBotId.avatar} alt="" />
              </div>
              <div className={style.loader}></div>
            </div>
          </>
        ) : null}
      </>
    );
  };

  return (
    <>
      <Snackbar
        open={open}
        autoHideDuration={3000}
        onClose={handleCloseCopy}
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <SnackbarContent
          action={action}
          message="Copied To Clipboard"
          sx={{ bgcolor: "white", color: "black" }}
        />
      </Snackbar>
      <div ref={divRef} className={style.container_mainChat}>
        {renderConversation()}
      </div>
    </>
  );
}

export default ContentChat;
