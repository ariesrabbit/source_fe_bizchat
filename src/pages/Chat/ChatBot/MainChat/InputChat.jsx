import React, { useEffect, useRef, useState } from "react";
import style from "./inputChat.module.css";
import { Icon } from "../../../../assets/icon_svg/icon";
import { chatService } from "../../../../services/ChatService";
import { useDispatch, useSelector } from "react-redux";
import {
  setConversation,
  setThinking,
  setQuestion,
  setAppear,
} from "../../../../redux/reduxToolkit/chatSlice";
import { idUserLocal } from "../../../../services/localServices";
const ResTimeOut =
  "I’m sorry for the delay in my response. It seems like I'm experiencing a technical issue, but our team is doing their best to resolve it. Please refresh the page and try again. Thank you for your patience!";

function InputChat() {
  const dispatch = useDispatch();
  const [question, setQuestionText] = useState("");
  const textAreaRef = useRef(null);
  const thinking = useSelector((state) => state.chatSlice.thinking);
  const newQuestion = useSelector((state) => state.chatSlice.question);
  const newAnswer = useSelector((state) => state.chatSlice.answer);
  const newOnAppear = useSelector((state) => state.chatSlice.onAppear);
  const detailBotId = useSelector((state) => state.chatSlice.detailBotId);
  const disable = useSelector((state) => state.chatSlice.disable);
  const showHeader = useSelector((state) => state.pageSlice.showHeader);

  useEffect(() => {
    if (newAnswer.length > 0) {
      dispatch(setAppear(false));
      dispatch(
        setConversation({
          type: "awnser",
          text: newAnswer,
          links: [],
          isExpend: false,
          isLoadLink: false,
        })
      );
    }
  }, [newAnswer]);

  useEffect(() => {
    if (newQuestion) setQuestionText(newQuestion);
  }, [newQuestion]);

  let getAnswer = async (requirement) => {
    try {
      let res = await chatService.postChat(requirement);
      if (res) {
        //console.log(res.reference_link)
        dispatch(
          setConversation({
            type: "awnser",
            text: res.text.length > 0 ? res.text : ResTimeOut,
            links: res.reference_link,
            topic_suggestions: res.topic_suggestions,
            quizgame: res.quizgame,
            isExpend: false,
            isLoadLink: false,
          })
        );
      } else {
        dispatch(
          setConversation({
            type: "awnser",
            text: ResTimeOut,
            links: [],
            topic_suggestions: [],
            quizgame: false,
            isExpend: false,
            isLoadLink: false,
          })
        );
      }
      dispatch(setThinking(false));
      dispatch(setQuestion(""));
    } catch (error) {}
  };

  const onEnterPress = (e) => {
    if (e.keyCode === 13 && !e.shiftKey) {
      if (question.trim() == "") return;
      dispatch(setThinking(true));
      dispatch(
        setConversation({
          type: "question",
          text: question,
        })
      );
      setQuestionText("");
      getAnswer({
        query: question,
        bot_id: detailBotId.id,
        user_id: idUserLocal.get(),
      });
    }
  };

  const onChangeInputText = (e) => {
    setQuestionText(e.target.value);
  };

  const getRows = () => {
    const lineCount = (question.match(/\n/g) || []).length + 1;
    return Math.min(lineCount, 3);
  };
  return (
    <div
      style={showHeader ? { alignItems: "center" } : { alignItems: "end" }}
      className={style.container_inputChat}
    >
      <div className={style.inputForm}>
        <textarea
          disabled={thinking || disable ? true : false}
          value={newOnAppear ? question : ""}
          className={style.inputChat}
          placeholder="Ask something"
          onChange={onChangeInputText}
          onKeyDown={onEnterPress}
          rows={getRows()}
          style={{ height: "auto", resize: "none" }}
        />
      </div>
      <div
        style={showHeader ? { alignItems: "center" } : { alignItems: "end" }}
        className={style.button}
      >
        <button
          onClick={() => {
            if (thinking) return;
            if (question.trim() === "") return;
            dispatch(setThinking(true));
            dispatch(
              setConversation({
                type: "question",
                text: question,
              })
            );
            setQuestionText("");
            getAnswer({
              query: question,
              user_id: idUserLocal.get(),
              bot_id: detailBotId.id,
            });
          }}
        >
          {Icon.Send}
        </button>
      </div>
    </div>
  );
}

export default InputChat;
