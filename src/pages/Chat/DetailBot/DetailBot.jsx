import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setQuestion, setAnswer } from "../../../redux/reduxToolkit/chatSlice";
import style from "./detailBot.module.css";
import { Icon } from "../../../assets/icon_svg/icon";
import { chatService } from "../../../services/ChatService";
import { idUserLocal } from "../../../services/localServices";

function DetailBot() {
  let dispatch = useDispatch();
  const suggestionMess = useSelector((state) => state.chatSlice.suggestionMess);
  const detailBotId = useSelector((state) => state.chatSlice.detailBotId);
  const thinking = useSelector((state) => state.chatSlice.thinking);
  const botId = useSelector((state) => state.chatSlice.botId);
  const conversation = useSelector((state) => state.chatSlice.conversation);

  const [show, setShow] = useState(false);
  return (
    <>
      {conversation?.length === 0 ? (
        <div className={`${style.bot_detail}  ${style.loading}`}></div>
      ) : (
        <div className={style.bot_detail}>
          <div className={style.infoBot}>
            <img src={detailBotId.avatar} className={style.avatarBot} alt="" />
            <p>{detailBotId.name}</p>
            {window.innerWidth <= 900 ? (
              <div className="flex flex-1 justify-end items-center pr-5">
                <div
                  onClick={() => {
                    setShow(!show);
                  }}
                  className={
                    show
                      ? `${style.plus} ${style.icon_show}`
                      : `${style.x} ${style.icon_show}`
                  }
                >
                  {Icon.Plus}
                </div>
              </div>
            ) : null}
            {window.innerWidth <= 900 ? null : (
              <div className={style.please}>
                Please give these features a try!
              </div>
            )}
          </div>

          {show ? (
            <div className={style.suggestion}>
              {suggestionMess.data?.map((item, index) => (
                <div
                  onClick={() => {
                    if (thinking) return;
                    if (item.is_injected) {
                      chatService.postInjectBotMemory({
                        user_id: idUserLocal.get(),
                        feature_type: item.name,
                        bot_id: botId,
                      });
                    }
                    dispatch(setAnswer(item.bot_say));
                    dispatch(setQuestion(item.user_input));
                    setShow(false);
                  }}
                  key={index}
                  className={style.question}
                >
                  <div className={style.img_icon}>
                    <img className="h-6 w-6" src={item.icon} alt="" />
                  </div>
                  <span>{item.name}</span>
                </div>
              ))}
            </div>
          ) : null}
          {window.innerWidth <= 900 ? null : (
            <div className={style.suggestion}>
              {suggestionMess.data?.map((item, index) => (
                <div
                  onClick={() => {
                    if (thinking) return;
                    if (item.is_injected) {
                      chatService.postInjectBotMemory({
                        user_id: idUserLocal.get(),
                        feature_type: item.name,
                        bot_id: botId,
                      });
                    }
                    dispatch(setAnswer(item.bot_say));
                    dispatch(setQuestion(item.user_input));
                    setShow(false);
                  }}
                  key={index}
                  className={style.question}
                >
                  <div className={style.img_icon}>
                    <img className="h-6 w-6" src={item.icon} alt="" />
                  </div>
                  <span>{item.name}</span>
                </div>
              ))}
            </div>
          )}
        </div>
      )}
    </>
  );
}

export default DetailBot;
