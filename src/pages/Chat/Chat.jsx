import React, { useEffect } from "react";
import ChatBot from "./ChatBot/ChatBot";
import style from "./chat.module.css";
import DetailBot from "./DetailBot/DetailBot";
import { useParams, useNavigate } from "react-router-dom";
import { chatService } from "../../services/ChatService";
import {
  setBotId,
  setConversation,
  setDetailBotId,
  setSuggestionMess,
} from "../../redux/reduxToolkit/chatSlice";
import { useDispatch, useSelector } from "react-redux";
import { pathLocal } from "../../services/localServices";
import { setIndexPage } from "../../redux/reduxToolkit/pageSlice";
import { idUserLocal } from "../../services/localServices";
import SlideBar from "../../components/SlideBar/SlideBar";

function Chat() {
  const { param_bot_id } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const showHeader = useSelector((state) => state.pageSlice.showHeader);
  let url_backend = useSelector((state) => state.configSlice.url_backend);
  let toString = (arr_chat, arr_topic) => {
    const newArr = [];
    for (let i = 0; i < arr_chat.length; i++) {
      newArr.push(arr_chat[i]);
    }
    dispatch(
      setConversation({
        type: "awnser",
        text: newArr.join("\n"),
        links: [],
        topic_suggestions: arr_topic,
        isExpend: false,
        isLoadLink: false,
      })
    );
  };
  let fetchApi = async () => {
    try {
      let res1 = await chatService.postBotIdByBot_id({
        bot_id: param_bot_id,
      });
      if (res1.data.length === 0) {
        dispatch(setIndexPage(1));
        // navigate("/bot_not_found");
        return;
      } else {
        try {
          let res2 = await chatService.postDetailBot({
            bot_id: res1.data[0].id,
            message_start_time: "2023-03-29T07:38:02.373Z",
            message_end_time: "2023-03-29T07:38:02.373Z",
          });

          let res3 = await chatService.postWelcomeMsg({
            user_id: idUserLocal.get(),
            bot_id: res1.data[0].id,
            day: 1,
            limit: 2,
          });

          dispatch(setBotId(res1.data[0].id));
          dispatch(setIndexPage(1));
          dispatch(setDetailBotId(res2.data));
          toString(res3.data.welcome_msg, res3.data.topic_suggestions);
          dispatch(setSuggestionMess(res2.data.chat_suggestions));
          pathLocal.set(res2.data.bot_id);
        } catch (error) {
          console.log("error: ", error);
        }
      }
    } catch (error) {
      console.log("error: ", error);
    }
  };

  useEffect(() => {
    if (url_backend.length > 0) {
      fetchApi();
    }
  }, [url_backend]);

  return (
    <>
      <SlideBar />
      <div className={style.container}>
        <div
          style={showHeader ? { height: "92vh" } : { height: "100vh" }}
          className={style.container_chat}
        >
          <ChatBot />
          <DetailBot />
        </div>
      </div>
    </>
  );
}

export default Chat;
