import React, { useEffect, useRef, useState } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { Icon } from "../../assets/icon_svg/icon";
import style from "./home.module.css";
import ReactGA from "react-ga";
import Welcome from "./Welcome/Welcome";
import SlideBar from "../../components/SlideBar/SlideBar";

function Home() {
  const [activeTab, setActiveTab] = useState(0);
  const navigate = useNavigate();

  useEffect(() => {
    ReactGA.pageview(window.location.pathname);
  }, []);

  const handleTabClick = (tabIndex) => {
    setActiveTab(tabIndex);
  };

  return (
    <>
      <SlideBar />
      <div className={style.container_home}>
        <div className={style.home}>
          <Welcome />
        </div>
      </div>
    </>
  );
}

export default Home;
