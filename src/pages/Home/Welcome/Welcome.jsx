import React, { useEffect, useRef, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Icon } from "../../../assets/icon_svg/icon";
import style from "./welcome.module.css";
import { desc } from "./welcome.module.css";
import { chatService } from "../../../services/ChatService";
import { setIndexPage } from "../../../redux/reduxToolkit/pageSlice";
import { pathLocal } from "../../../services/localServices";
import {
  resetConversation,
  setConversation,
} from "../../../redux/reduxToolkit/chatSlice";

function Welcome() {
  let dispatch = useDispatch();
  const refSlide1 = useRef();
  const refSlide2 = useRef();
  const [botList, setBotList] = useState([]);
  const [lastest, setLastest] = useState([]);

  const navigate = useNavigate();
  let disable = useSelector((state) => state.chatSlice.disable);
  let url_backend = useSelector((state) => state.configSlice.url_backend);
  let fetch = async () => {
    let res1 = await chatService.getRecentBots();
    let res2 = await chatService.getTopMess();
    setBotList(res1.data);
    setLastest(res2.data);
  };
  const next1 = () => {
    if (disable) return;
    refSlide1.current.slickNext();
  };
  const previous1 = () => {
    if (disable) return;
    refSlide1.current.slickPrev();
  };
  const next2 = () => {
    if (disable) return;
    refSlide2.current.slickNext();
  };
  const previous2 = () => {
    if (disable) return;
    refSlide2.current.slickPrev();
  };
  const settings1 = {
    className: "h-full w-[92%] mx-auto",
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: botList.length < 6 ? botList.length : 6,
    slidesToScroll: botList.length < 6 ? botList.length : 6,
    arrows: false,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1279,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false,
          arrows: false,
          speed: 700,
        },
      },
      {
        breakpoint: 767,
        settings: {
          dots: false,
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          initialSlide: 0,
          arrows: false,
          speed: 500,
        },
      },
    ],
  };
  const settings2 = {
    className: "h-full w-[92%] mx-auto",
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: false,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1279,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: false,
          arrows: false,
          speed: 700,
        },
      },
      {
        breakpoint: 767,
        settings: {
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          initialSlide: 0,
          arrows: false,
          speed: 500,
        },
      },
    ],
  };

  const fetch_get_bot_id_by_token = async () => {
    try {
      const res = await chatService.getBotByToken();
      // console.log(res.data)
    } catch (error) {
      // console.log(error)
    }
  };

  useEffect(() => {
    if (url_backend) {
      fetch();
      // fetch_get_bot_id_by_token()
    }
  }, [url_backend]);
  const renderListBot = () => {
    return botList?.map((bot, index) => {
      return (
        <div key={bot.id} className={style.container_bot_detail}>
          <div className={style.bot_detail}>
            <div className={style.avatar}>
              <img src={bot.avatar} alt="bot avatar" />
            </div>
            <div className={style.content}>
              <div className="flex flex-col justify-center items-center">
                <span className={style.botName}>
                  {bot.name?.length > 16
                    ? bot.name.substring(0, 12) + "..."
                    : bot.name}
                </span>
                <span className={style.botId}>@{bot.bot_id}</span>
                <div className={style.botDesc}>{bot.description}</div>
              </div>
              <div className="flex justify-center">
                <button
                  className="mb-[12px]"
                  onClick={() => {
                    pathLocal.set(bot.bot_id);
                    dispatch(setIndexPage(1));
                    navigate(`/${bot.bot_id}`);
                    dispatch(resetConversation());
                  }}
                >
                  Chat
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    });
  };
  const renderLastestChats = () => {
    return lastest?.map((last, index) => {
      return (
        <div key={index} className={style.container_lastest}>
          <div className={style.lastest_detail}>
            <div className={style.lastest}>
              <div className="flex">
                <div className="w-[40px] h-[40px]">
                  <img
                    src={last.avatar}
                    className="rounded-full w-full h-full"
                    alt=""
                  />
                </div>
                <div className="ml-[10px]">
                  <div className={style.lastest_name}>{last.name}</div>
                  <div className={style.botId}>@{last.bot_id}</div>
                </div>
              </div>
              <div className={style.lastest_answer}>{last.anwser}</div>
            </div>
          </div>
        </div>
      );
    });
  };

  return (
    <div className={style.sroll}>
      <div className={style.container_welcome}>
        <div
          // style={{ backgroundImage: `url(${imgbanner})` }}
          className={style.welcome_head}
        >
          <h1 className={style.big_text}>24/7 Personal Assistant</h1>
          <p className={style.small_text}>
            A helper, a friend, a mentor, an entertainer, a virtual learning
            assistant. I can literally be anything! Let your creativity shine!
          </p>
        </div>
      </div>

      <div className={style.list_bot}>
        <div className={style.welcome_text}>
          {Icon.Fire2} <span className="ml-2">Welcome characters</span>
        </div>
        <div className="relative mx-auto">
          <div
            className={`${style.arrows} absolute w-full top-32 flex justify-between`}
          >
            <div onClick={previous1} className={style.arrows_right}>
              {Icon.ArrRight}
            </div>
            <div onClick={next1} className={style.arrows_left}>
              {Icon.ArrLeft}
            </div>
          </div>
          {disable ? (
            <div
              className={`${style.loading} h-[270px] w-[94%] mx-auto `}
            ></div>
          ) : (
            <Slider ref={refSlide1} {...settings1}>
              {renderListBot()}
            </Slider>
          )}
        </div>
      </div>
      <div>
        <div className={style.latest_text}>
          {Icon.BoxChat}
          <span className="ml-2">Latest answers</span>
        </div>

        <div className="relative mx-auto mt-[24px] ">
          <div
            className={`${style.arrows} absolute w-full top-[40%] flex justify-between`}
          >
            <div onClick={previous2} className={style.arrows_right}>
              {Icon.ArrRight}
            </div>
            <div onClick={next2} className={style.arrows_left}>
              {Icon.ArrLeft}
            </div>
          </div>
          {disable ? (
            <div
              className={`${style.loading} h-[190px] w-[94%] mx-auto `}
            ></div>
          ) : (
            <Slider ref={refSlide2} {...settings2}>
              {renderLastestChats()}
            </Slider>
          )}
        </div>
      </div>
      <div className="h-10"></div>
    </div>
  );
}

export default Welcome;
