import React, { useEffect, useRef, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Icon } from "../../../assets/icon_svg/icon";
import style from "./bodyPage.module.css";
import { chatService } from "../../../services/ChatService";
import { setIndexPage } from "../../../redux/reduxToolkit/pageSlice";
import { pathLocal } from "../../../services/localServices";
import { setConversation } from "../../../redux/reduxToolkit/chatSlice";
import imgEgg from "../../../assets/imgEgg.png";
import illu1 from "../../../assets/personal-illustration-1.png";
import illu2 from "../../../assets/personal-illustration-2.png";
import illu3 from "../../../assets/personal-illustration-3.png";
import buss1 from "../../../assets/business-image-1.png";
import buss2 from "../../../assets/business-image-2.png";
import buss3 from "../../../assets/business-image-3.png";
import buss4 from "../../../assets/business-image-4.png";
import logoAI from "../../../assets/OpenAI-logo.png";
import logoApache from "../../../assets/logo-apache.png";

function BodyPage() {
  let dispatch = useDispatch();
  const refSlide1 = useRef();
  const [botList, setBotList] = useState([]);

  const navigate = useNavigate();
  let disable = useSelector((state) => state.chatSlice.disable);
  let url_backend = useSelector((state) => state.configSlice.url_backend);
  let fetch = async () => {
    let res1 = await chatService.getRecentBots();
    setBotList(res1.data);
  };
  const next1 = () => {
    if (disable) return;
    refSlide1.current.slickNext();
  };
  const previous1 = () => {
    if (disable) return;
    refSlide1.current.slickPrev();
  };

  const settings1 = {
    className: "h-full w-[92%] mx-auto",
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: botList.length < 6 ? botList.length : 6,
    slidesToScroll: botList.length < 6 ? botList.length : 6,
    arrows: false,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1279,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false,
          arrows: false,
          speed: 700,
        },
      },
      {
        breakpoint: 767,
        settings: {
          dots: false,
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          initialSlide: 0,
          arrows: false,
          speed: 500,
        },
      },
    ],
  };

  useEffect(() => {
    if (url_backend) {
      fetch();
      // fetch_get_bot_id_by_token()
    }
  }, [url_backend]);
  const renderListBot = () => {
    return botList?.map((bot, index) => {
      return (
        <div key={bot.id} className={style.container_bot_detail}>
          <div className={style.bot_detail}>
            <div className={style.avatar}>
              <img src={bot.avatar} alt="bot avatar" />
            </div>
            <div className={style.content}>
              <div className="flex flex-col justify-center items-center">
                <span className={style.botName}>
                  {bot.name?.length > 16
                    ? bot.name.substring(0, 12) + "..."
                    : bot.name}
                </span>
                <span className={style.botId}>@{bot.bot_id}</span>
                <div className={style.botDesc}>{bot.description}</div>
              </div>
              <div className="flex justify-center">
                <button
                  className="mb-[12px]"
                  onClick={() => {
                    pathLocal.set(bot.bot_id);
                    dispatch(setIndexPage(1));
                    navigate(`/${bot.bot_id}`);
                  }}
                >
                  Chat
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    });
  };

  return (
    <div className="mt-10">
      <div className="w-full text-center mb-[40px]">
        <p className={style.section_text}>Try chatting</p>
        <div className="w-full flex justify-center">{Icon.VectorGreen}</div>
      </div>
      <div className={style.list_bot}>
        <div className="relative mx-auto">
          <div
            className={`${style.arrows} absolute w-full top-32 flex justify-between`}
          >
            <div onClick={previous1} className={style.arrows_right}>
              {Icon.ArrRight}
            </div>
            <div onClick={next1} className={style.arrows_left}>
              {Icon.ArrLeft}
            </div>
          </div>
          {disable ? (
            <div
              className={`${style.loading} h-[270px] w-[94%] mx-auto `}
            ></div>
          ) : (
            <Slider ref={refSlide1} {...settings1}>
              {renderListBot()}
            </Slider>
          )}
        </div>
      </div>
      <div className="w-full text-center mb-[40px]">
        <p className={style.section_text}>What it does for personal ?</p>
        <div className="w-full flex justify-center">{Icon.VectorGreen}</div>
      </div>
      <div className="flex flex-col xl:flex-row mb-24">
        <div className="w-full xl:w1/2 flex justify-center items-center">
          <div className="w-4/5 mx-auto">
            <img src={imgEgg} alt="" />
          </div>
        </div>
        <div className="w-full xl:w1/2 flex justify-center items-start">
          <div className="pr-0 xl:pr-14">
            <div className="flex w-4/5 mx-auto xl:w-full gap-10 mt-[44px]">
              {window.innerWidth <= 900 ? null : (
                <div>
                  <img src={illu1} alt="" />
                </div>
              )}
              <div>
                <p className={style.text_big}>Helper on the go</p>
                <p className={style.text_medium}>
                  Ask anything and get instant response 24/7
                </p>
                <p className={style.text_small}>
                  There may be times when you need to complete a task or make a
                  decision quickly, and having a personal helper who can provide
                  instant responses can be very helpful.
                </p>
              </div>
            </div>
            <div className="flex w-4/5 mx-auto xl:w-full gap-10 mt-[44px]">
              {window.innerWidth <= 900 ? null : (
                <div>
                  <img src={illu2} alt="" />
                </div>
              )}
              <div>
                <p className={style.text_big}>Mentor or Life coach</p>
                <p className={style.text_medium}>
                  Offer emotional support like a friend
                </p>
                <p className={style.text_small}>
                  Billybot give life guidance tailoring to user preferences.
                  Depending on your skills and knowledge, you may be able to get
                  expert advice or recommendations on various topics.
                </p>
              </div>
            </div>
            <div className="flex w-4/5 mx-auto xl:w-full gap-10 mt-[44px]">
              {window.innerWidth <= 900 ? null : (
                <div>
                  <img src={illu3} alt="" />
                </div>
              )}
              <div>
                <p className={style.text_big}>Personality and Humor</p>
                <p className={style.text_medium}>
                  Provide emotional conversations
                </p>
                <p className={style.text_small}>
                  Customize replied tones and share your newly created bots with
                  the world, entertainment and and witty responses to make
                  conversations enjoyable.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="w-full text-center mb-[40px]">
        <p className={style.section_text}>What it does for business ?</p>
        <div className="w-full flex justify-center">{Icon.VectorGreen}</div>
      </div>
      <div className="mx-auto grid grid-cols-1 xl:grid-cols-4 w-[95%] gap-5 xl:gap-0">
        <div className="mx-auto w-[95%] flex flex-col justify-start rounded-[16px] bg-[#F8F8F8] p-[12px]">
          <div>
            <img className="h-48 w-full rounded-lg" src={buss1} alt="" />
            <p className={style.text_big_buss}>24/7 Customer Service</p>
            <p className={style.text_small_buss}>
              Handle customer enquiries, anytime, anywhere. Personalize
              notifications based on user feedback
            </p>
          </div>
        </div>
        <div className="mx-auto w-[95%] flex flex-col justify-start rounded-[16px] bg-[#F8F8F8] p-[12px]">
          <div>
            <img className="h-48 w-full rounded-lg" src={buss2} alt="" />
            <p className={style.text_big_buss}>Optimal sales boost</p>
            <p className={style.text_small_buss}>
              Segment customers and tailor offerings across various social
              networks
            </p>
          </div>
        </div>
        <div className="mx-auto w-[95%] flex flex-col justify-start rounded-[16px] bg-[#F8F8F8] p-[12px]">
          <div>
            <img className="h-48 w-full rounded-lg" src={buss3} alt="" />
            <p className={style.text_big_buss}>Company Hub</p>
            <p className={style.text_small_buss}>
              Cross-departmental knowledge sharing, brainstorm ideas and
              streamline communication
            </p>
          </div>
        </div>
        <div className="mx-auto w-[95%] flex flex-col justify-start rounded-[16px] bg-[#F8F8F8] p-[12px]">
          <div>
            <img className="h-48 w-full rounded-lg" src={buss4} alt="" />
            <p className={style.text_big_buss}>Personal Assistant</p>
            <p className={style.text_small_buss}>
              Learn literally anything! Integrate with all tools, prompt and get
              instant answers
            </p>
          </div>
        </div>
      </div>
      <div className="mt-24">
        <p className={style.power_text}>Powered by</p>
        <div className="flex flex-row xl:mb-[44px]">
          <div className="w-1/2 flex justify-end mr-4">
            <img className="h-12" src={logoApache} alt="" />
          </div>
          <div className="w-1/2 flex justify-start ml-4">
            <img className="h-12" src={logoAI} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default BodyPage;
