import React, { useEffect, useRef, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { Icon } from "../../assets/icon_svg/icon";
import style from "./landingPage.module.css";
import ReactGA from "react-ga";
import BodyPage from "./Body/BodyPage";
import Banner from "./Banner/Banner";
import Footer from "./Footer/Footer";

function LandingPage() {
  return (
    <div className={style.container}>
      <div className={style.container_home}>
        <Banner />
        <BodyPage />
        <Footer />
      </div>
    </div>
  );
}

export default LandingPage;
