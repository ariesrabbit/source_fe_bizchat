import React, { useEffect, useState } from "react";
import banner from "../../../assets/Banner.png";
import style from "./banner.module.css";
import { useNavigate } from "react-router-dom";

export default function Banner() {
  let navigate = useNavigate();
  const [text, setText] = useState("");
  const dataText = [
    "your learning journey",
    "business solutions",
    "your searching",
  ];
  useEffect(() => {
    setTimeout(() => {
      const typeWriter = (text, i, fnCallback) => {
        if (i < text.length) {
          setText(text.substring(0, i + 1));
          setTimeout(() => {
            typeWriter(text, i + 1, fnCallback);
          }, 50);
        } else if (typeof fnCallback === "function") {
          setTimeout(fnCallback, 700);
        }
      };

      const startTextAnimation = (i) => {
        if (typeof dataText[i] === "undefined") {
          setTimeout(() => {
            startTextAnimation(0);
          }, 500);
        } else if (i < dataText[i].length) {
          typeWriter(dataText[i], 0, () => {
            startTextAnimation(i + 1);
          });
        }
      };

      startTextAnimation(0);
    }, 1800);
  }, []);

  return (
    <div className={style.banner}>
      <div className="w-full xl:max-w-2/3">
        <div
          className={`${style.animated} ${style.animatedFadeInUp} ${style.fadeInUp} `}
        >
          <div className={style.welcome_text_big}>
            <p>
              Elevate {window.innerWidth <= 900 ? <br /> : null}
              <span className={style.gradient_text}>{text}</span>
              <br />
              with our supportive bot
            </p>
          </div>
        </div>

        <div className={`${style.welcome_text_small} w-full sm:w-2/3 xl:w-1/2`}>
          We bring together all the best practices for lifelong learners:
          personalization, spontaneity, progression, interconnection. Invent
          your future of learning and take them to new heights.
        </div>
        <div className={style.welcome_button}>
          <button
            onClick={() => {
              navigate("/home");
            }}
          >
            Try a beta
          </button>
        </div>
      </div>
    </div>
  );
}
