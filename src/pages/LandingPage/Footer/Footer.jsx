import React from "react";
import logoFooter from "../../../assets/BizChatLogo.png";
import style from "./footer.module.css";
import { Icon } from "../../../assets/icon_svg/icon";

export default function Footer() {
  return (
    <div id="footer" className={style.footer}>
      <div className="w-[95%] mx-auto pt-[40px] pb-[32px]">
        <div className="w-full flex flex-col xl:flex-row">
          <div className="flex w-full xl:w-1/2">
            <div className="w-1/2">
              <div>
                <img className="h-10" src={logoFooter} alt="" />
              </div>
              <div className="mt-[15px]">
                <p className={style.small_text_footer}>
                  Decentralize domain-based knowledge. Get connected with the
                  world
                </p>
              </div>
            </div>
            <div className="w-1/2">
              <div className={style.medium_text_footer}>Contact us</div>
              <div className="mt-3 flex gap-4">
                <div className="h-10 w-10 bg-white rounded-full cursor-pointer">
                  {Icon.FB}
                </div>
                <div className="h-10 w-10 bg-white rounded-full cursor-pointer">
                  {Icon.IN}
                </div>
              </div>
            </div>
          </div>
          <div className="w-full xl:w-1/2 mt-3 xl:mt-0">
            <div className={style.medium_text_footer}>Subscribe</div>
            <div className="mt-[6px]">
              <p className={style.small_text_footer}>
                Joining the waitlist to ensure that you don't miss out on any
                important updates or releases, get our the inside scoop on new
                products, special offers, and more.
              </p>
            </div>
            <div className="mt-5 w-full flex items-center justify-start gap-4">
              <div className="w-full">
                <input placeholder="Your email" type="text" />
              </div>
              <div>
                <button>Subscribe</button>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-10">© 2023 BizChat</div>
      </div>
    </div>
  );
}
