import React from "react";
import style from "./errorPage.module.css";

export default function ErrorPage() {
  return (
    <div className="h-screen w-screen flex justify-center items-center">
      Bot not found, please try again !!!
    </div>
  );
}
