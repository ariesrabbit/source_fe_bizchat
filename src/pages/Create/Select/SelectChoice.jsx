import Select from "react-select";
import React, { useState } from "react";

export default function SelectChoice({ options, isMulti, placeholder }) {
  const [selectedOptions, setSelectedOptions] = useState([]);
  const handleSelectChange = (selectedOptions) => {
    const values = selectedOptions.map((option) => option.value);
    setSelectedOptions(values);
  };

  const customStyles = {
    control: (provided, state) => ({
      ...provided,
      borderColor: state.isFocused ? "#37CDBC" : provided.borderColor,
      boxShadow: state.isFocused ? "0 0 0 1px #37CDBC" : provided.boxShadow,
      "&:hover": {
        borderColor: "#37CDBC",
      },
      padding: "3px",
    }),
    placeholder: (provided, state) => ({
      ...provided,
      color: "#919191", // change the color to whatever you want
      fontSize: "15px",
      fontFamily: "Mulish",
      fontWeight: "400",
      lineHeight: "19px",
    }),
  };
  const customTheme = (theme) => ({
    ...theme,
    colors: {
      ...theme.colors,
      primary: "#37CDBC",
      primary25: "#F7F7FD",
      primary50: "#F0F2F3",
      primary75: "#F0F2F3",
      danger: "#000",
      dangerLight: "#37CDBC",
    },
  });

  return (
    <div>
      <Select
        placeholder={placeholder}
        className="mt-2"
        isMulti={isMulti}
        id="characteristics"
        options={options}
        styles={customStyles}
        theme={customTheme}
        // onChange={handleSelectChange}
      />
    </div>
  );
}
