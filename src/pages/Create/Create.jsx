import React, { useState } from "react";
import avt from "../../assets/avatars/avtclone.png";
import { Icon } from "../../assets/icon_svg/icon";
import style from "./create.module.css";
import { useEffect } from "react";
import SelectChoice from "./Select/SelectChoice";
import InputForm from "./Input/InputForm";
import SlideBar from "../../components/SlideBar/SlideBar";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import {
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
} from "@mui/material";
import LinkUploadModal from "./Modal/LinkUpload/LinkUpload";
import LinkUpload from "./Modal/LinkUpload/LinkUpload";
import FileUpload from "./Modal/FileUpload/FileUpload";
const styleModal = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};
export default function Create() {
  const [imageUrl, setImageUrl] = useState(avt);
  const [contentBio, setContentBio] = useState("");
  const [contentGreeting, setContentGreeting] = useState("");
  const [content, setContent] = useState("");
  const [selectingHashtag, setselectingHashtag] = useState([]);

  function readURL(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = function (e) {
        setImageUrl(e.target.result);
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  //
  const optionChars = [
    { value: "Humorously", label: "Humorously" },
    { value: "Thoughtful", label: "Thoughtful" },
    { value: "Professionally", label: "Professionally" },
  ];
  const optionsTone = [
    { value: "Creative", label: "Creative" },
    { value: "Neutral", label: "Neutral" },
    { value: "Accurate", label: "Accurate" },
  ];
  const optionsLanguages = [
    { value: "English", label: "English" },
    { value: "Vietnamese", label: "Vietnamese" },
  ];
  //

  //hashtags
  const hashtags = [
    "Economy",
    "Technology",
    "Sport",
    "Cuisine",
    "Art",
    "Entertainment",
    "Social",
    "Education",
    "Health",
    "Law",
    "Tourism",
    "Games",
    "Horoscope",
    "Other",
  ];
  const handleAddHashtag = (hashtag) => {
    if (selectingHashtag.includes(hashtag)) {
      // If the hashtag already exists, remove it
      const updatedHashtags = selectingHashtag.filter((h) => h !== hashtag);
      setselectingHashtag(updatedHashtags.slice(0, 3));
    } else {
      // If the hashtag does not exist, add it
      const updatedHashtags = [...selectingHashtag, hashtag];
      setselectingHashtag(updatedHashtags.slice(0, 3));
    }
  };
  //

  //Visibility
  //  Modal
  const [openFileUploadModal, setOpenFileUploadModal] = useState(false);
  const [openTextUploadModal, setOpenTextUploadModal] = useState(false);

  const handleCloseFileUploadModal = () => {
    setOpenFileUploadModal(false);
  };

  const handleCloseTextUploadModal = () => {
    setOpenTextUploadModal(false);
  };
  //

  return (
    <>
      <SlideBar />
      <div className={style.container_create}>
        <div className="w-full mx-auto xl:w-[70%]">
          <div className={style.avatar_upload}>
            <div className={style.avatar_edit}>
              <input
                type="file"
                id="imageUpload"
                accept=".png, .jpg, .jpeg"
                onChange={readURL}
              />
              <label htmlFor="imageUpload">
                <div className="w-full h-full flex justify-center items-center">
                  {Icon.Upload}
                </div>
              </label>
            </div>
            <div className={style.avatar_preview}>
              <div
                id="imagePreview"
                style={{ backgroundImage: `url(${imageUrl})` }}
              ></div>
            </div>
          </div>
          <div className="w-full flex flex-col xl:flex-row justify-between mt-12">
            <div className="w-full xl:w-[49%]">
              <div>
                <div className={style.label}>First name*</div>
                <input
                  type="text"
                  className="mt-2 border border-[#C6C6C6] text-gray-900 text-sm rounded-[6px] focus:outline-[#37CDBC] hover:border-[#37CDBC] w-full p-2.5"
                  placeholder=""
                />
              </div>
            </div>
            <div className="w-full xl:w-[49%]">
              <div>
                <div className={style.label}>Billy ID*</div>
                <input
                  type="text"
                  className=" mt-2 border border-[#C6C6C6] text-gray-900 text-sm rounded-[6px] focus:outline-[#37CDBC] hover:border-[#37CDBC] w-full p-2.5"
                  placeholder=""
                />
              </div>
            </div>
          </div>
          <div className="w-full mt-10">
            <div>
              <div className={style.label}>Characteristics</div>
            </div>
            <SelectChoice
              options={optionChars}
              isMulti={true}
              placeholder={`Select characteristic`}
            />
          </div>
          <div className="w-full mt-10">
            <div>
              <div className={style.label}>Bio</div>
            </div>
            <InputForm
              value={contentBio}
              onChange={setContentBio}
              placeholder={`Describe the purpose and features of your bot in a clear and concise way to make your bot more relatable and interesting.`}
            />
          </div>
          <div className="w-full mt-10">
            <div>
              <div className={style.label}>Greeting *</div>
            </div>
            <InputForm
              value={contentGreeting}
              onChange={setContentGreeting}
              placeholder={`Consider including the bot's name in the greeting to make it feel more personalized. For example, 'Hi, I'm [bot name]. What can I help you with?`}
            />
          </div>
          <div className="w-full mt-10">
            <div>
              <div className={style.label}>Tone & Voice</div>
              <SelectChoice
                options={optionsTone}
                isMulti={false}
                placeholder={`Select the tone of your bot`}
              />
            </div>
          </div>
          <div className="w-full mt-10">
            <div>
              <div className={style.label}>Default language</div>
              <SelectChoice
                options={optionsLanguages}
                isMulti={false}
                placeholder={`Select your bot language`}
              />
            </div>
          </div>
          <div className="w-full mt-10">
            <div>
              <div className={style.label}>Hashtags</div>
              <div className={style.sub}>Select 3 hashtags for your bot</div>
            </div>
            <div>
              <div className="flex flex-wrap gap-[10px] mt-10">
                {hashtags.map((hashtag) => {
                  return (
                    <div
                      onClick={() => {
                        handleAddHashtag(hashtag);
                      }}
                      className={`${style.hashtag} ${
                        selectingHashtag.includes(hashtag)
                          ? style.selected
                          : null
                      }`}
                      key={hashtag}
                    >
                      #{hashtag}
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <div className="w-full mt-10">
            <div>
              <div className={style.label}>Visibility</div>
            </div>
            <div>
              <FormControl>
                <RadioGroup>
                  <FormControlLabel
                    value="public"
                    control={
                      <Radio
                        sx={{
                          color: "#C6C6C6",
                          "&.Mui-checked": {
                            color: "#4246CC",
                          },
                        }}
                      />
                    }
                    label={
                      <>
                        <span className={style.content}>Public </span>
                        <span
                          style={{
                            color: "#C6C6C6",
                            fontSize: "15px",
                          }}
                        >
                          (Anyone can discover and chat with your bot via search
                          or shared link)
                        </span>
                      </>
                    }
                  />
                  <FormControlLabel
                    value="private"
                    control={
                      <Radio
                        sx={{
                          color: "#C6C6C6",
                          "&.Mui-checked": {
                            color: "#4246CC",
                          },
                        }}
                      />
                    }
                    label={
                      <>
                        <span className={style.content}>Private</span>
                      </>
                    }
                  />
                </RadioGroup>
              </FormControl>
            </div>
          </div>
          <div className="w-full mt-10">
            <div>
              <div className={style.label}>
                Is there any documents you want the bot to learn?
              </div>
              <div className={style.sub}>
                <div className="flex gap-2">
                  Elevate your bot's domain knowledge by uploading files and
                  URLs.
                  <div className="cursor-pointer">{Icon.CircleI}</div>
                </div>
              </div>
              <div className="mt-5">
                <div className="flex flex-col gap-2 items-start">
                  <button
                    onClick={() => setOpenTextUploadModal(true)}
                    className={style.popup_button}
                  >
                    {Icon.Link}Import URL
                  </button>

                  <button
                    onClick={() => setOpenFileUploadModal(true)}
                    className={style.popup_button}
                  >
                    {Icon.UploadMini}Upload file
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="w-full mt-10">
            <div>
              <div className={style.label}>
                <div className="flex items-center gap-2">
                  {Icon.Phone} Contact me
                </div>
              </div>
              <div className={style.sub}>
                <div className="flex gap-2">
                  Share your contact information with those who are interested
                  in your bot and wish to connect with you.
                </div>
              </div>
            </div>
            <InputForm
              value={content}
              onChange={setContent}
              placeholder={`Example:\nYou can contact me following below information:\nEmail:\nWebsite:`}
            />
          </div>
          <div className="mt-12 flex justify-center">
            <button className={style.btn_create}>Create</button>
          </div>
          <div className="h-12"></div>
        </div>
      </div>
      <LinkUpload
        open={openTextUploadModal}
        onClose={handleCloseTextUploadModal}
      />
      <FileUpload
        open={openFileUploadModal}
        onClose={handleCloseFileUploadModal}
      />
    </>
  );
}
