import React from "react";
import style from "./inputForm.module.css";

export default function InputForm({ value, onChange, placeholder }) {
  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      onChange(value + "\n");
    }
  };
  return (
    <div className={style.inputForm}>
      <textarea
        id="greeting"
        value={value}
        className={style.input}
        placeholder={placeholder}
        onChange={(event) => onChange(event.target.value)}
        onKeyDown={handleKeyDown}
        rows={5}
        style={{ height: "auto", resize: "none" }}
      />
    </div>
  );
}
