import { useState } from "react";
import { Box, Modal } from "@mui/material";
import { Icon } from "../../../../assets/icon_svg/icon";
import "./linkUpload.css";

const styleModal = {
  borderRadius: "20px",
  bgcolor: "#fff",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  p: "16px",
  width: window.innerWidth <= 900 ? "90%" : "35%",
};
function LinkUpload({ open, onClose }) {
  return (
    <Modal open={open} onClose={onClose}>
      <Box sx={styleModal}>
        <div>Import URL</div>
        <div className="flex">
          <div className="flex justify-center items-center">
            {Icon.LightBold}
          </div>
          <div className="hint">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </div>
        </div>
        <div className="container_uplink">
          <input
            className="w-full"
            type="text"
            placeholder="Paste your link here"
          />
          <button className="popup_button">{Icon.Link}Import URL</button>
        </div>
      </Box>
    </Modal>
  );
}

export default LinkUpload;
