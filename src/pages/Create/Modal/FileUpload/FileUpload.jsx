import { useEffect, useRef, useState } from "react";
import "./fileUpload.css";
import { useDispatch, useSelector } from "react-redux";
import Box from "@mui/material/Box";
import { LinearProgress, Modal } from "@mui/material";
import { Icon } from "../../../../assets/icon_svg/icon";

const styleModal = {
  borderRadius: "20px",
  bgcolor: "#fff",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  p: "16px",
  width: window.innerWidth <= 900 ? "90%" : "35%",
};
function FileUpload({ open, onClose }) {
  let dispatch = useDispatch();
  const wrapperRef = useRef(null);
  const divRef = useRef(null);
  const [fileList, setFileList] = useState([]);
  const [crrFile, setCrrFile] = useState({});
  const [uploading, setUploading] = useState(false);

  const onDragOver = (e) => {
    e.preventDefault();
    wrapperRef.current.classList.add("dragover");
  };
  const onDragLeave = () => {
    wrapperRef.current.classList.remove("dragover");
  };
  const onDrop = (e) => {
    e.preventDefault();
    const newFiles = Array.from(e.dataTransfer.files);
    setFileList([...fileList, ...newFiles]);
    wrapperRef.current.classList.remove("dragover");
  };

  const onFileDrop = (e) => {
    const newFile = e.target.files[0];

    if (newFile) {
      setUploading(true);
      setTimeout(() => {
        setFileList([...fileList, newFile]);
      }, 1000);
      setCrrFile(newFile);
    }
    setTimeout(() => {
      setUploading(false);
    }, 1000);
  };
  const fileRemove = (file) => {
    const updatedList = fileList.filter((item) => item !== file);
    setFileList(updatedList);
  };
  const formatFileSize = (size) => {
    const units = ["B", "KB", "MB", "GB"];
    let unitIndex = 0;
    while (size >= 1024 && unitIndex < units.length - 1) {
      size /= 1024;
      unitIndex++;
    }
    return `${size.toFixed(2)} ${units[unitIndex]}`;
  };

  useEffect(() => {
    if (divRef.current) {
      divRef.current.scrollTop = divRef.current.scrollHeight;
    }
  }, [fileList]);

  return (
    <Modal open={open} onClose={onClose}>
      <Box sx={styleModal}>
        <div className="flex justify-between items-start">
          <div className="upload_text big">Upload file</div>
          <div onClick={onClose} className="cursor-pointer">
            {Icon.XDelete}
          </div>
        </div>
        <div
          ref={wrapperRef}
          className="drop_file_input"
          onDragOver={onDragOver}
          onDragLeave={onDragLeave}
          onDrop={onDrop}
        >
          <div className="w-full flex flex-col justify-center items-center">
            <div>{Icon.UploadFileBig}</div>
            <div className="upload_text">
              Drop and drag file or <span>choose file </span>
            </div>
          </div>
          <input type="file" value="" onChange={onFileDrop} />
        </div>
        <div className="mt-2 mb-4">
          <div className="sub_text">Format: pdf, cvs, txt</div>
        </div>
        {uploading && (
          <div className="process">
            <div className="w-11/12">
              <div className="break-words">{crrFile?.name}</div>
              <div className="flex justify-between">
                <div>
                  {crrFile?.size ? formatFileSize(crrFile?.size) : null}
                </div>
                {/* <div>{uploadProgress}%</div> */}
              </div>
              <div className="progress-bar-container">
                <LinearProgress
                  color="inherit"
                  sx={{ color: "#6C6FD7", backgroundColor: "#F8F8F8" }}
                />
              </div>
            </div>
            <div className="flex justify-center items-center mr-3">
              {Icon.XDelete}
            </div>
          </div>
        )}
        {fileList.length > 0 && (
          <div ref={divRef} className="max-h-52 overflow-auto">
            <div className="drop_file_preview">
              {fileList.map((item, index) => (
                <div key={index} className="preview_item">
                  <div className="flex justify-between items-start gap-6">
                    <div className="preview_item_info">
                      <div>{Icon.ImgFile}</div>
                      <p className="file_name">{item.name}</p>
                      <p className="file_size">{formatFileSize(item.size)}</p>
                    </div>
                    <div
                      className="preview_item_del"
                      onClick={() => fileRemove(item)}
                    >
                      {Icon.XDelete}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
        <div className="flex justify-end gap-2 mt-6">
          <button
            onClick={() => {
              setFileList([]);
              onClose();
            }}
            className="btn cancel"
          >
            Cancel
          </button>
          <button
            onClick={() => {
              onClose();
            }}
            className="btn done"
          >
            Done
          </button>
        </div>
      </Box>
    </Modal>
  );
}

export default FileUpload;
