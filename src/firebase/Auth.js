// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
import { getRemoteConfig } from "firebase/remote-config";
import { getAuth, signInWithPopup, GoogleAuthProvider } from "firebase/auth";


const firebaseConfig = {
  apiKey: "AIzaSyBmxc1yGcUKdaEm4kq0WdZczD74PEyRhJs",
  authDomain: "login-demo-de440.firebaseapp.com",
  projectId: "login-demo-de440",
  storageBucket: "login-demo-de440.appspot.com",
  messagingSenderId: "553696352682",
  appId: "1:553696352682:web:0780f9b3217403f0d533d3",
  measurementId: "G-NXCH5J72L2"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const provider = new GoogleAuthProvider();
provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
const auth = getAuth();
auth.languageCode = 'it';

provider.setCustomParameters({
  'login_hint': 'user@example.com'
});


const remoteConfig = getRemoteConfig(app);
remoteConfig.defaultConfig = {
  welcome_message: "Welcome",
};
remoteConfig.settings.minimumFetchIntervalMillis = 3000;

export {remoteConfig,auth,provider};

