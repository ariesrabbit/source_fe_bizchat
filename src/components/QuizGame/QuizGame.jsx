import React, { useEffect, useRef, useState } from "react";
import { Icon } from "../../assets/icon_svg/icon";
import style from "./quizgame.module.css";
import { useDispatch, useSelector } from "react-redux";

export default function QuizGame({ quiz, idQuiz }) {
  const conversation = useSelector((state) => state.chatSlice.conversation);

  const newArr = conversation.filter((item) => {
    return item.type === "quizgame";
  });

  const [selectedAnswers, setSelectedAnswers] = useState([]);
  const [correctAnswers, setCorrectAnswers] = useState([]);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const explanationRef = useRef(null);

  const handleAnswerClick = (questionIndex, answerIndex) => {
    setSelectedAnswers((prevSelectedAnswers) => {
      const newSelectedAnswers = [...prevSelectedAnswers];
      newSelectedAnswers[questionIndex] = answerIndex;
      return newSelectedAnswers;
    });
    setCorrectAnswers((prevCorrectAnswers) => {
      const newCorrectAnswers = [...prevCorrectAnswers];
      const isCorrect = quiz[questionIndex].options[answerIndex].answer;
      newCorrectAnswers[questionIndex] = isCorrect
        ? answerIndex
        : quiz[questionIndex].options.findIndex((option) => option.answer);
      return newCorrectAnswers;
    });
  };

  useEffect(() => {
    const objWithCurrentId = newArr.find((obj) => obj.id === idQuiz);
    if (objWithCurrentId) {
      const maxIdObj = newArr.reduce(
        (max, obj) => (obj.id > max.id ? obj : max),
        { id: 0 }
      );
      if (maxIdObj === objWithCurrentId) {
        if (explanationRef.current) {
          const padding = 80;
          explanationRef.current.scrollIntoView({
            behavior: "smooth",
            block: "start",
            inline: "nearest",
          });
          window.scrollBy({ top: -padding, left: 0, behavior: "smooth" });
        }
      } else {
        return;
      }
    } else {
      return;
    }
  }, [selectedAnswers, currentQuestionIndex]);

  return (
    <div>
      {quiz[currentQuestionIndex] && (
        <div key={currentQuestionIndex}>
          <div className="ml-1">{quiz[currentQuestionIndex].quiz}</div>
          <div>
            {quiz[currentQuestionIndex].options.map((option, optionIndex) => (
              <button
                key={optionIndex}
                className={`${style.quiz_choices} 
                  ${
                    selectedAnswers[currentQuestionIndex] === optionIndex
                      ? option.answer
                        ? style.true
                        : style.false
                      : correctAnswers[currentQuestionIndex] === optionIndex
                      ? style.true
                      : ""
                  }`}
                onClick={() => {
                  handleAnswerClick(currentQuestionIndex, optionIndex);
                }}
                disabled={selectedAnswers[currentQuestionIndex] !== undefined}
              >
                {option.text}
              </button>
            ))}
          </div>
          {selectedAnswers[currentQuestionIndex] !== undefined ? (
            <div ref={explanationRef} className="mt-[16px] w-full ml-1">
              {quiz[currentQuestionIndex].explain}
            </div>
          ) : null}
        </div>
      )}
      {quiz?.length <= 1 ? (
        <div className="mb-3"></div>
      ) : (
        <div className={style.next_pre}>
          <button
            onClick={() =>
              setCurrentQuestionIndex((prevIndex) =>
                prevIndex > 0 ? prevIndex - 1 : prevIndex
              )
            }
            disabled={currentQuestionIndex === 0}
          >
            <div className="mr-2">{Icon.VectorLeft}</div>
            Previous
          </button>

          <div className="h-full flex justify-center items-center">
            {currentQuestionIndex + 1}/{quiz.length}
          </div>

          <button
            onClick={() => {
              setCurrentQuestionIndex((prevIndex) =>
                prevIndex < quiz.length - 1 ? prevIndex + 1 : prevIndex
              );
            }}
            disabled={currentQuestionIndex === quiz.length - 1}
          >
            Next
            <div className="ml-2">{Icon.VectorRight}</div>
          </button>
        </div>
      )}
    </div>
  );
}
