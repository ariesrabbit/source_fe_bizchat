import React from "react";
import BizLogo from "../../assets/BizChatLogo.png";
import ScrollHeader from "../../HOC/Scroll/ScrollHeader";
import style from "./headerIframe.module.css";

const HeaderIframe = () => {
  const scrollDirection = ScrollHeader();

  return (
    <div
      className={`${style.header} sticky ${
        scrollDirection === "down" ? "-top-24" : "top-0"
      }`}
    >
      <div className={style.container_header}>
        <div className={style.logo}>
          <img src={BizLogo} alt="" />
        </div>
      </div>
    </div>
  );
};

export default HeaderIframe;
