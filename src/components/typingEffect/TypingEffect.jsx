import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import {
  setPropConversation,
  setAppear,
  setQuestion,
} from "../../redux/reduxToolkit/chatSlice";
const TypingEffect = ({ divRef, content, idx, topicArr }) => {
  const dispatch = useDispatch();
  const [text, setText] = useState("");
  const messages = [];
  messages.push(content);

  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    if (text === content) {
      dispatch(setPropConversation(idx));
      dispatch(setAppear(true));
      return;
    }
    if (divRef) {
      divRef.current.scrollTop = divRef.current.scrollHeight;
    }

    const interval = setInterval(() => {
      setText(messages[currentIndex].substring(0, text.length + 1));
    }, 10);

    return () => clearInterval(interval);
  }, [currentIndex, text, messages]);

  return <p className="break-words">{text}</p>;
};

export default TypingEffect;
