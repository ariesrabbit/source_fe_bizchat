import React, { useEffect, useState } from "react";
import gif from "../../assets/bot.gif";
import bot_popup from "../../assets/bot_popup.png";
import style from "./modalPopup.module.css";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setIndexPage, setShowModal } from "../../redux/reduxToolkit/pageSlice";
import { getAuth, signInWithPopup, GoogleAuthProvider } from "firebase/auth";
import { auth, provider } from "../../firebase/Auth";
import { chatService } from "../../services/ChatService";
import { useCookies } from "react-cookie";
import { setAccessTokenPage } from "../../redux/reduxToolkit/configSlice";
import { setUser } from "../../redux/reduxToolkit/userSlice";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import login_biz from "../../assets/login_biz.png";
import { Icon } from "../../assets/icon_svg/icon";
import { idUserLocal } from "../../services/localServices";

const styleModal = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: window.innerWidth <= 900 ? "90%" : "45%",
};
export default function ModalLogin() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [cookies, setCookie] = useCookies(["access_token", "refresh_token"]);
  const showModal = useSelector((state) => state.pageSlice.showModal);

  const handleClose = () => {
    dispatch(setShowModal(false));
  };

  const postRegisterWith3rd = async (user) => {
    const formdata = {
      name: user.displayName.toString(),
      avatar: user.photoURL,
      uid: user.uid.toString(),
      email: user.email.toString(),
      display_name: user.displayName.toString(),
      email_verify: user.emailVerified,
      is_anonymous: user.isAnonymous,
      access_token: user.accessToken.toString(),
      refresh_token: user.stsTokenManager.refreshToken.toString(),
      expire_in: user.stsTokenManager.expirationTime,
      create_at: user.metadata.createdAt.toString(),
      last_login_at: user.metadata.lastLoginAt.toString(),
      phone_number: user.phoneNumber ? user.phoneNumber.toString() : " ",
      metadata: JSON.stringify(user.metadata),
    };
    try {
      const res = await chatService.postRegisterWith3rd(formdata);
      console.log("res: ", res);
      idUserLocal.set(res.data.id);
      dispatch(
        setUser({
          avatarUrl: formdata.avatar,
          isLogin: true,
        })
      );
      dispatch(setShowModal(false));
    } catch (error) {
      console.log(error);
    }
  };

  const handleLoginGoogle = async () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        postRegisterWith3rd(user);
        let expires = new Date();
        expires.setTime(
          expires.getTime() + user.stsTokenManager.expirationTime * 1000
        );
        setCookie("access_token", user.accessToken.toString(), {
          path: "/",
          expires,
        });
        setCookie(
          "refresh_token",
          user.stsTokenManager.refreshToken.toString(),
          { path: "/", expires }
        );
        // IdP data available using getAdditionalUserInfo(result)
        // ...
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.customData.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ...
      });
  };
  return (
    <div className={style.container_modal}>
      <Modal open={showModal} onClose={handleClose}>
        <Box sx={styleModal}>
          <div className="h-full bg-white rounded-[20px] relative">
            <div
              onClick={() => {
                handleClose();
              }}
              className="absolute top-4 right-4 cursor-pointer z-10"
            >
              {Icon.Cancel}
            </div>
            <div className="flex">
              <div className="w-1/3">
                <img
                  className="rounded-s-[20px] h-full"
                  src={bot_popup}
                  alt=""
                />
              </div>
              <div className="w-2/3 flex flex-col justify-center items-center">
                <div className="flex justify-center mb-2">
                  <img className="h-12" src={login_biz} alt="" />
                </div>
                <div className="flex justify-center mb-4">
                  <p className={style.signin_text}>
                    Sign in to create your companion
                  </p>
                </div>

                <div className="bg-white shadow w-2/3 mx-auto">
                  <button
                    onClick={handleLoginGoogle}
                    aria-label="Continue with google"
                    role="button"
                    className="py-2 border flex justify-center items-center w-full"
                  >
                    <svg
                      width={19}
                      height={20}
                      viewBox="0 0 19 20"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M18.9892 10.1871C18.9892 9.36767 18.9246 8.76973 18.7847 8.14966H9.68848V11.848H15.0277C14.9201 12.767 14.3388 14.1512 13.047 15.0812L13.0289 15.205L15.905 17.4969L16.1042 17.5173C17.9342 15.7789 18.9892 13.221 18.9892 10.1871Z"
                        fill="#4285F4"
                      />
                      <path
                        d="M9.68813 19.9314C12.3039 19.9314 14.4999 19.0455 16.1039 17.5174L13.0467 15.0813C12.2286 15.6682 11.1306 16.0779 9.68813 16.0779C7.12612 16.0779 4.95165 14.3395 4.17651 11.9366L4.06289 11.9465L1.07231 14.3273L1.0332 14.4391C2.62638 17.6946 5.89889 19.9314 9.68813 19.9314Z"
                        fill="#34A853"
                      />
                      <path
                        d="M4.17667 11.9366C3.97215 11.3165 3.85378 10.6521 3.85378 9.96562C3.85378 9.27905 3.97215 8.6147 4.16591 7.99463L4.1605 7.86257L1.13246 5.44363L1.03339 5.49211C0.37677 6.84302 0 8.36005 0 9.96562C0 11.5712 0.37677 13.0881 1.03339 14.4391L4.17667 11.9366Z"
                        fill="#FBBC05"
                      />
                      <path
                        d="M9.68807 3.85336C11.5073 3.85336 12.7344 4.66168 13.4342 5.33718L16.1684 2.59107C14.4892 0.985496 12.3039 0 9.68807 0C5.89885 0 2.62637 2.23672 1.0332 5.49214L4.16573 7.99466C4.95162 5.59183 7.12608 3.85336 9.68807 3.85336Z"
                        fill="#EB4335"
                      />
                    </svg>
                    <p className="text-base font-medium ml-4 text-gray-700">
                      Continue with Google
                    </p>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Box>
      </Modal>
    </div>
  );
}
