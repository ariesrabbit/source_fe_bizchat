import React, { useEffect, useState } from "react";
import style from "./slideBar.module.css";
import { Icon } from "../../assets/icon_svg/icon";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { setIndexPage } from "../../redux/reduxToolkit/pageSlice";
import { pathLocal } from "../../services/localServices";
import ScrollHeader from "../../HOC/Scroll/ScrollHeader";
import { resetConversation } from "../../redux/reduxToolkit/chatSlice";

function SlideBar() {
  let dispatch = useDispatch();
  let hide = useSelector((state) => state.pageSlice.hide);
  let is_nightly = useSelector((state) => state.configSlice.is_nightly);
  const tabData = [
    { icon: Icon.Home, name: "Home", navigate: "/home" },
    { icon: Icon.Chat, name: "Chat", navigate: `/${pathLocal.get()}` },
    // { icon: Icon.Create, name: "Create", navigate: "/create" },
  ];
  const [activeTab, setActiveTab] = useState(0);
  const scrollDirection = ScrollHeader();

  let indexPage = useSelector((state) => state.pageSlice.indexPage);
  const navigate = useNavigate();
  const handleTabClick = (tabIndex) => {
    // setActiveTab(tabIndex);
    dispatch(setIndexPage(tabIndex));
  };

  useEffect(() => {
    setActiveTab(indexPage);
  }, [indexPage]);
  useEffect(() => {
    const path = window.location.pathname;
    if (path.indexOf("/:param_bot_id") !== -1) {
      dispatch(setIndexPage(1));
    }
  }, []);
  return (
    <>
      {window.innerWidth <= 900 ? (
        <div
          style={
            hide
              ? { transform: "translateX(-300px)" }
              : { transform: "translateX(0px)" }
          }
          className={`${style.slidebar} ${style.light}`}
        >
          {is_nightly ? (
            <div className={style.container_slidebar}>
              <div className={`${style.icon_slidebar} ${style.active_tab}`}>
                <span>{tabData[1].icon}</span>
                <span>{tabData[1].name}</span>
              </div>
            </div>
          ) : (
            <div className={style.container_slidebar}>
              {tabData.map((tab, index) => (
                <div
                  className={
                    activeTab === index
                      ? `${style.icon_slidebar} ${style.active_tab} ${style.light}`
                      : `${style.icon_slidebar}`
                  }
                  key={index}
                  onClick={() => {
                    handleTabClick(index);
                    navigate(tab.navigate);
                  }}
                >
                  <span>{tab.icon}</span>
                  <span>{tab.name}</span>
                </div>
              ))}
            </div>
          )}
        </div>
      ) : (
        <div
          className={` ${`${style.slidebar} ${style.light}`}
              ${scrollDirection === "down" ? "-top-0" : "top-19"}`}
        >
          {is_nightly ? (
            <div className={style.container_slidebar}>
              <div className={`${style.icon_slidebar} ${style.active_tab}`}>
                <span>{tabData[1].icon}</span>
                <span>{tabData[1].name}</span>
              </div>
            </div>
          ) : (
            <div className={`${style.container_slidebar} `}>
              {tabData.map((tab, index) => (
                <div
                  className={
                    activeTab === index
                      ? `${style.icon_slidebar} ${style.active_tab} ${style.light}`
                      : `${style.icon_slidebar}`
                  }
                  key={index}
                  onClick={() => {
                    handleTabClick(index);
                    navigate(tab.navigate);
                    dispatch(resetConversation());
                  }}
                >
                  <span>{tab.icon}</span>
                  <span>{tab.name}</span>
                </div>
              ))}
            </div>
          )}
        </div>
      )}
    </>
  );
}

export default SlideBar;
