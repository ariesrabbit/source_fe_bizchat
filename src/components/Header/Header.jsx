import React, { useEffect, useState } from "react";
import { Logo, Icon } from "../../assets/icon_svg/icon";
import style from "./header.module.css";
import BizLogo from "../../assets/BizChatLogo.png";
import { useDispatch, useSelector } from "react-redux";
import {
  setHide,
  setIndexPage,
  setShowModal,
} from "../../redux/reduxToolkit/pageSlice";
import ModalLogin from "../modalLogin/ModalLogin";
import { chatService } from "../../services/ChatService";
import Avatar from "@mui/material/Avatar";
import { setUser } from "../../redux/reduxToolkit/userSlice";
import { useCookies } from "react-cookie";
import { Box, Menu, MenuItem, Tooltip, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { idUserLocal } from "../../services/localServices";
import ScrollHeader from "../../HOC/Scroll/ScrollHeader";

function Header() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let hide = useSelector((state) => state.pageSlice.hide);
  let avatarUrl = useSelector((state) => state.userSlice.avatarUrl);
  let isLogin = useSelector((state) => state.userSlice.isLogin);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const url_backend = useSelector((state) => state.configSlice.url_backend);
  const [onClick, setOnClick] = useState(false);
  const scrollDirection = ScrollHeader();

  const [token, setToken, removeToken] = useCookies([
    "access_token",
    "refresh_token",
  ]);

  // ###############

  const handleLogOut = () => {
    removeToken(["access_token"]);
    removeToken(["refresh_token"]);
    window.location.reload(true);
    idUserLocal.remove();
  };

  // ###############
  const auth_access_token = async () => {
    try {
      if (!token["access_token"]) return;
      const res = await chatService.getAuthToken();
      if (res.data) {
        dispatch(
          setUser({
            avatarUrl: res.data.avatar,
            isLogin: true,
          })
        );
      } else {
      }
    } catch (error) {}
  };

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  useEffect(() => {
    if (url_backend) {
      auth_access_token();
    }
  }, [url_backend]);

  return (
    <div
      className={`${style.header} sticky ${
        scrollDirection === "down" ? "-top-24" : "top-0"
      }`}
    >
      <div className={style.container_header}>
        <div
          onClick={() => {
            dispatch(setIndexPage(0));
            navigate("/");
          }}
          className={style.logo}
        >
          <img src={BizLogo} alt="" />
        </div>
        <div className="flex h-full justify-end items-end">
          <ModalLogin />

          <div className={style.login}>
            {isLogin ? (
              <>
                <div className="mr-3">
                  <Box sx={{ flexGrow: 0 }}>
                    <Tooltip title="Open settings">
                      <span onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                        <Avatar alt="" src={avatarUrl} />
                      </span>
                    </Tooltip>
                    <Menu
                      sx={{ mt: "45px" }}
                      id="menu-appbar"
                      anchorEl={anchorElUser}
                      anchorOrigin={{
                        vertical: "top",
                        horizontal: "right",
                      }}
                      keepMounted
                      transformOrigin={{
                        vertical: "top",
                        horizontal: "right",
                      }}
                      open={Boolean(anchorElUser)}
                      onClose={handleCloseUserMenu}
                    >
                      <MenuItem
                        onClick={() => {
                          handleCloseUserMenu();
                          handleLogOut();
                        }}
                      >
                        <Typography textAlign="center">Logout</Typography>
                      </MenuItem>
                    </Menu>
                  </Box>
                </div>
              </>
            ) : (
              <>
                <button
                  onClick={() => {
                    setOnClick(!onClick);
                    dispatch(setShowModal(true));
                  }}
                >
                  Login
                </button>
              </>
            )}
          </div>

          {onClick ? <ModalLogin setPage={0} nav={"/"}></ModalLogin> : null}
          {window.innerWidth <= 900 ? (
            <div
              onClick={() => {
                dispatch(setHide(!hide));
              }}
              className={style.openhide}
            >
              {hide ? Icon.Opentab : Icon.Hidetab}
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
}

export default Header;
