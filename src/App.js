import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import { useDispatch, useSelector } from "react-redux";
import SlideBar from "./components/SlideBar/SlideBar";
import Chat from "./pages/Chat/Chat";
import Home from "./pages/Home/Home";
import ErrorPage from "./pages/NotFound/ErrorPage";
import { idUserLocal } from "./services/localServices";
import ReactGA from "react-ga";
import "./App.css";
import Create from "./pages/Create/Create";
import LandingPage from "./pages/LandingPage/LandingPage";
import { getValue } from "firebase/remote-config";
import { fetchAndActivate } from "firebase/remote-config";
import { useEffect, useState } from "react";
import { remoteConfig } from "./firebase/Auth";
import { setConfigPage } from "./redux/reduxToolkit/configSlice";
import chatSlice from "./redux/reduxToolkit/chatSlice";
import { chatService, getCookie } from "./services/ChatService";
import { useCookies } from "react-cookie";
import { async } from "@firebase/util";
import HeaderIframe from "./components/HeaderIframe/HeaderIframe";
import { setShowHeader } from "./redux/reduxToolkit/pageSlice";
// import { config } from "./firebase/Auth";
const TRACKING_ID = "G-D7HQ3B52R1";
ReactGA.initialize(TRACKING_ID);
function App() {
  let dispatch = useDispatch();
  let url_backend = useSelector((state) => state.configSlice.url_backend);
  const access_token = getCookie("access_token");
  const anonymous_id = (minLen, maxLen) =>
    +Array.from(
      { length: 0 | (Math.random() * (maxLen - minLen + 1) + minLen) },
      () => 0 | (Math.random() * 9 + 1)
    ).join("");

  const [token, setToken, removeToken] = useCookies([
    "access_token",
    "refresh_token",
  ]);

  const apply_config = (config) => {
    dispatch(setConfigPage(config));
  };

  const RefreshToken = async () => {
    // console.log(token["refresh_token"])
    if (!token["refresh_token"]) return;
    const res = await chatService.getRefreshToken({
      refresh_token: token["refresh_token"],
    });
    // console.log(res)
  };

  useEffect(() => {
    if (!access_token) {
      let user_id = idUserLocal.get();
      if (!user_id || user_id == -1) {
        idUserLocal.set(anonymous_id(5, 5));
      }
    }
  }, []);
  useEffect(() => {
    // console.log(document.cookie)
    fetchAndActivate(remoteConfig)
      .then(() => {
        const config = JSON.parse(
          getValue(remoteConfig, "CONFIG_DYNAMIC_FE")["_value"]
        );
        apply_config(config[process.env.REACT_APP_MODE]);
      })
      .catch((err) => {});
    // console.log("Done fetch")
  }, []);

  useEffect(() => {
    if (url_backend) {
      RefreshToken();
      // const interval = setInterval(() => {
      //   RefreshToken()
      // }, 5000);
    }
  }, [url_backend]);
  const showHeader = useSelector((state) => state.pageSlice.showHeader);
  useEffect(() => {
    // Check if the website is being loaded in an iframe
    if (window.self !== window.top) {
      // The website is being loaded in an iframe
      // Add code here to handle the iframe
      dispatch(setShowHeader(false));
    } else {
      // The website is not being loaded in an iframe
      // Add code here to handle the main window
    }
  }, []);

  return (
    <div>
      <Router>
        {showHeader ? <Header /> : null}
        <Routes>
          <Route exact path="/" element={<LandingPage />}></Route>
          <Route exact path="/home" element={<Home />}></Route>
          <Route exact path="/:param_bot_id?" element={<Chat />}></Route>
          <Route exact path="/create" element={<Create />}></Route>
          <Route exact path="/bot_not_found" element={<ErrorPage />}></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
