import { createSlice } from "@reduxjs/toolkit";

const initialState = { isDark: false };
// true is dark mode

const themeSlice = createSlice({
  name: "themeSlice",
  initialState,
  reducers: {
    setTheme(state, { payload }) {
      state.isDark = payload;
    },
  },
});

export const { setTheme } = themeSlice.actions;
export default themeSlice.reducer;
