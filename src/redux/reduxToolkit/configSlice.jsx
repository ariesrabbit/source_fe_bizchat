import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  is_nightly: "",
  url_backend: "",
  access_token: "",
  refresh_token : ""
};

const configSlice = createSlice({
  name: "configSlice",
  initialState,
  reducers: {
    setConfigPage(state, { payload }) {
      state.is_nightly = payload["is_nightly"]
      state.url_backend = payload["url_backend"]
    },
    setAccessTokenPage(state, { payload }) {
      state.access_token = payload["access_token"]
      state.refresh_token = payload["refresh_token"]
    }
  },
});

export const { setConfigPage, setAccessTokenPage } = configSlice.actions;

export default configSlice.reducer;
