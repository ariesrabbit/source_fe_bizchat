import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  detailBotId: {},
  suggestionMess: {},
  conversation: [],
  thinking: false,
  question: "",
  answer: "",
  onAppear: true,
  disable: true,
  count: 0,
  botId: "",
  arrIdQuiz: [],
};
let nextId = 0;
const chatSlice = createSlice({
  name: "chatSlice",
  initialState,
  reducers: {
    setDisable(state, { payload }) {
      state.count++;
      state.disable = true;
    },
    setEnable(state, { payload }) {
      state.count--;
      if (state.count == 0) {
        state.disable = false;
      }
    },
    setDetailBotId(state, { payload }) {
      state.detailBotId = payload;
      state.conversation = [];
    },
    setSuggestionMess(state, { payload }) {
      state.suggestionMess = payload;
    },
    setConversation(state, { payload }) {
      if (payload.id === -1) return;
      state.conversation.push({ ...payload, id: nextId++ });
    },
    resetConversation: (state) => {
      state.conversation = [];
    },
    setThinking(state, { payload }) {
      state.thinking = payload;
    },
    setQuestion(state, { payload }) {
      state.question = payload;
    },
    setAnswer(state, { payload }) {
      state.answer = payload;
    },
    setAppear(state, { payload }) {
      state.onAppear = payload;
    },
    setPropConversation(state, { payload }) {
      state.conversation.forEach((item, index) => {
        if (item.id === payload) {
          state.conversation[index].isLoadLink = true;
          return;
        }
      });
    },
    setPropExpendConversation(state, { payload }) {
      state.conversation.forEach((item, index) => {
        if (item.id === payload) {
          state.conversation[index].isExpend = !item.isExpend;
          return;
        }
      });
    },
    setBotId(state, { payload }) {
      state.botId = payload;
    },
    setArrIdQuiz(state, { payload }) {
      state.arrIdQuiz = [...payload];
    },
  },
});

export const {
  setDetailBotId,
  setSuggestionMess,
  setConversation,
  resetConversation,
  setThinking,
  setQuestion,
  setPropConversation,
  setPropExpendConversation,
  setAnswer,
  setAppear,
  setDisable,
  setEnable,
  setBotId,
  setArrIdQuiz,
} = chatSlice.actions;

export default chatSlice.reducer;
