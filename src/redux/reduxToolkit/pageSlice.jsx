import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  indexPage: 0,
  hide: true,
  showModal: false,
  showHeader: true,
};

const pageSlice = createSlice({
  name: "pageSlice",
  initialState,
  reducers: {
    setIndexPage(state, { payload }) {
      state.indexPage = payload;
    },
    setHide(state, { payload }) {
      state.hide = payload;
    },
    setShowModal(state, { payload }) {
      state.showModal = payload;
    },
    setShowHeader(state, { payload }) {
      state.showHeader = payload;
    },
  },
});

export const { setIndexPage, setHide, setShowModal, setShowHeader } =
  pageSlice.actions;

export default pageSlice.reducer;
