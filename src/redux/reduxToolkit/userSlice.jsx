import { createSlice } from "@reduxjs/toolkit";
import { idUserLocal } from "../../services/localServices";

let initialState = {
  avatarUrl: "",
  isLogin: false,
  idUser: idUserLocal.get(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUser(state, { payload }) {
      // console.log( payload["avatarUrl"])
      state.avatarUrl = payload["avatarUrl"];
      state.isLogin = payload["isLogin"];
    },
    setIdUser(state, { payload }) {
      state.idUser = payload;
    },
  },
});

export const { setUser, setIdUser } = userSlice.actions;

export default userSlice.reducer;
