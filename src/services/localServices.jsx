import { getCookie } from "./ChatService";

const LIST_BOT = "LIST_BOT";
const ID_USER = "ID_USER";
const PATH = "PATH";

export const listBotLocal = {
  set: (listBot) => {
    let json = JSON.stringify(listBot);
    localStorage.setItem(LIST_BOT, json);
  },
  get: () => {
    let json = localStorage.getItem(LIST_BOT);
    if (json) {
      return JSON.parse(json);
    } else {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(LIST_BOT);
  },
};

export const idUserLocal = {
  set: (idUser) => {
    let json = JSON.stringify(idUser);
    localStorage.setItem(ID_USER, json);
  },
  get: () => {
    let json = localStorage.getItem(ID_USER);
    if (json) {
      return JSON.parse(json);
    } else {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(ID_USER);
  },
};

export const pathLocal = {
  set: (path) => {
    let json = JSON.stringify(path);
    localStorage.setItem(PATH, json);
  },
  get: () => {
    let json = localStorage.getItem(PATH);
    if (json) {
      return JSON.parse(json);
    } else {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(PATH);
  },
};
