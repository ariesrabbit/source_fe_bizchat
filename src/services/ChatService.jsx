import { https } from "./urlConfig";
import { store } from "..";

export const getCookie = (name) => {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
};

export const chatService = {
  // postChat: (question) => {
  //   let uri = "/api/v2/chats";
  //   return https.post(uri, question);
  // },
  postChat: (question) => {
    let uri = store.getState()["configSlice"]["url_backend"] + "/api/v3/chats";
    return https.post(uri, question);
  },
  postDetailBot: (params) => {
    let uri =
      store.getState()["configSlice"]["url_backend"] + "/api/v1/get-bot-detail";
    return https.post(uri, params);
  },
  postWelcomeMsg: (bot_id) => {
    let uri =
      store.getState()["configSlice"]["url_backend"] + "/api/v1/welcome-msg";
    return https.post(uri, bot_id);
  },
  postBotByUserId: (user_id) => {
    let uri =
      store.getState()["configSlice"]["url_backend"] +
      "/api/v1/get-bots-by-userId";
    return https.post(uri, user_id);
  },
  postBotIdByBot_id: (bot_id) => {
    let uri =
      store.getState()["configSlice"]["url_backend"] +
      "/api/v1/get-bot-by-bot_id";
    return https.post(uri, bot_id);
  },
  postInjectBotMemory: (params) => {
    let uri =
      store.getState()["configSlice"]["url_backend"] +
      "/api/v3/inject_bot_memory";
    return https.post(uri, params);
  },
  getTopMess: () => {
    let uri =
      store.getState()["configSlice"]["url_backend"] +
      "/api/v1/get-top-message";
    return https.get(uri);
  },
  getRecentBots: () => {
    let uri =
      store.getState()["configSlice"]["url_backend"] +
      "/api/v1/get-recent-bots";
    return https.get(uri);
  },
  postQuizGame: (params) => {
    let uri =
      store.getState()["configSlice"]["url_backend"] + "/api/v3/quiz_game";
    return https.post(uri, params);
  },
  postRegisterWith3rd: (params) => {
    let uri =
      store.getState()["configSlice"]["url_backend"] +
      "/api/v1/users/register-with-3rd";
    return https.post(uri, params);
  },
  getBotByToken: () => {
    let uri =
      store.getState()["configSlice"]["url_backend"] +
      "/api/v1/get-bots-by-token";
    return https.get(uri, {
      headers: {
        Authorization: `Bearer ${getCookie("access_token")}`,
      },
    });
  },
  getAuthToken: () => {
    let uri =
      store.getState()["configSlice"]["url_backend"] + "/api/v1/users/me";
    return https.get(uri, {
      headers: {
        Authorization: `Bearer ${getCookie("access_token")}`,
      },
    });
  },
  getRefreshToken: (params) => {
    let uri =
      store.getState()["configSlice"]["url_backend"] +
      "/api/v1/users/refresh-token";
    // console.log(params)
    // console.log(getCookie("access_token"))
    return https.post(uri, params, {
      headers: {
        Authorization: `Bearer ${getCookie("access_token")}`,
      },
    });
  },
};
