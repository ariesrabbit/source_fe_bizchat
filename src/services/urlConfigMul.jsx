import axios from "axios";

const BASE_URL = "https://test-be.billybot.ai";
export const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Content-Type": "application/json",
    // Authorization: `Bearer ${localStorage.getItem("tkn")}`,
  },
});

// Add a request interceptor
https.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    return { ...config };
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  // function (response) {
  //   // Any status code that lie within the range of 2xx cause this function to trigger
  //   // Do something with response data

  //   return response;
  // },
  // function (error) {
  //   // Any status codes that falls outside the range of 2xx cause this function to trigger
  //   // Do something with response error
  //   return Promise.reject(error);
  // }
  (response) => {
    if (response && response.data) return response.data;
    return response;
  },
  (err) => {
    // alert("Error 500")
    // throw err.response.data;
    return null;
  }
);
